# Sales Interaction

A simple mobile application I made with Xamarin for the Mobile Application Programming class. This app allows you to keep track of a salesperson's interactions with their customers regarding the products the salesperson is selling. You can add/remove customers from the database, as well as their interactions. There are three built-in products that a customer can buy: the Wonder Jacket, the Wonder Hat, and the Wonder Boots.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

What things you need to install the software and how to install them

 - **Visual Studio**: The IDE used to develop this application. Can be installed from here: https://www.visualstudio.com/.
	 - **Xamarin**: Extension for Visual Studio that allows the use of Xamarin to develop cross-platform mobile apps. Can be installed from here: https://www.visualstudio.com/xamarin/.

### Installing
*Note*: You can download a [pre-built APK](https://bitbucket.org/cjanzen/sales_interaction/downloads/Sales_Interaction.Android.apk) that you can install onto your Android device if you don't want to build the APK yourself. You may need to enable installations from Unknown Sources by tapping "Settings > Security" and checking "Unknown Sources" prior to installing the APK.

Ensure that Visual Studio and Xamarin are installed on your machine using the links above.

Once installed, you can open the "Sales_Interaction.sln" file to open the solution. Once opened, you may need to right click "Solution 'Sales_Interaction'" and click on "Restore Nuget Packages" to install any missing packages required for the project.

Once the packages are installed, you can run the project through the simulator/emulator or a physical device by pressing "Ctrl + F5". Note that in order to run the app on an iPhone, you will need to install the latest versions of Xamarin Mac and XCode on a Mac and will have to compile the code on a Mac.

### Usage
Once the app is running, the first page you will encounter is the **Customers** page. This allows you to add a new customer with their information, view a customer's interactions by tapping their name, and delete a customer by tapping and holding (or swiping, depending on the device) the customer and tapping the "Delete" button (this will also delete the customer's interactions).

When viewing a customer's interactions on the **Interactions** page, you can
 - Add interactions by entering information in the table at the bottom of the page.
 - Modify interactions by tapping an interaction and entering information in the table at the bottom of the page.
 - Delete interactions by tapping and holding (or swiping, depending on the device) an interaction and tapping the "Delete" button.

In the toolbar at the top of every page, you can access two more pages:

- **Products**: Shows a static list of products that can be sold to a customer. By default, there are three products: the Wonder Jacket, the Wonder Hat, and the Wonder Boots. Each item in the list shows the product's name, description, price, and the number of interactions associated with the product. Currently, the list and its items cannot be modified.
- **Settings**: Shows a single button that allows you to reset the database to its default state, which means all customers and interactions are removed, but not the products.

## Built With

* [Visual Studio](https://www.visualstudio.com/) - The IDE used to develop the app.
* [Xamarin](https://www.visualstudio.com/xamarin/) - An extension for Visual Studio that allows for cross-platform mobile application development using Native Compilation.
* [sqlite-net-pcl](https://github.com/praeclarum/sqlite-net) - Used for data persistence using SQLite and Object Relational Mapping (ORM).

## Authors

* **Corey Janzen**  - [cjanzen](https://bitbucket.org/cjanzen/)


> Written with [StackEdit](https://stackedit.io/).