﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Sales_Interaction.Cells;
using Sales_Interaction.Models;
using Sales_Interaction.ViewModels;
using Sales_Interaction.Views;
using Xamarin.Forms;

namespace Sales_Interaction.Pages
{
    /// <summary>
    /// The Page class that contains information about the interactions
    /// for a specific customer. Also allows users to change the properties
    /// for an interaction or add a new interaction.
    /// </summary>
    public class InteractionsPage : GeneralPage
	{
        /// <summary>Page title</summary>
        public const string TITLE = "Interactions";

        /// <summary>Contains a list of customers</summary>
        public ObservableCollection<InteractionProductCustomer> Interactions
        {
            get { return interactions; }
        }
        /// <summary>Contains a list of customers</summary>
        private ObservableCollection<InteractionProductCustomer> interactions;

        /// <summary>The view that contains the list of interactions</summary>
        public ListView List
        {
            get { return listView; }
        }
        /// <summary>The view that contains the list of interactions</summary>
        private ListView listView;

        /// <summary>
        /// Instantiates a page that displays all of a customer's interactions.
        /// Users can change the properties of an interaction or add a new one.
        /// </summary>
        /// <param name="customer">The customer this page relates to</param>
        public InteractionsPage (Customer customer)
		{
            /* Generate the interactions table */
            InteractionTable table = new InteractionTable(this, customer)
            {
                VerticalOptions = LayoutOptions.EndAndExpand
            };

            /* Generate a List View with the customers interactions */
            interactions = new ObservableCollection<InteractionProductCustomer>(
                App.Database.GetCustomersInteractions(customer));

            listView = new ListView
            {
                ItemsSource = interactions,
                ItemTemplate = new DataTemplate(typeof(InteractionsCell)),
                RowHeight = InteractionsCell.RowHeight,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            /* Setup the item tapped event */
            listView.ItemTapped += (sender, e) =>
            {
                // Get the selected IteractionProductCustomer
                InteractionProductCustomer selected = (InteractionProductCustomer)e.Item;

                // If the item actually exists in the database
                if (App.Database.Exists((selected.Interaction)))
                {
                    // Take the selected interaction and sent it to the table
                    table.Modify();
                }
                else
                {
                    // Otherwise, display an error message
                    DisplayNonExistantError();
                    // remove the selected item from the list
                    interactions.Remove(selected);
                }
            };
            // When an interaction is removed, reset the table and deselect
            // the current element
            interactions.CollectionChanged += (sender, e) =>
            {
                listView.SelectedItem = null;
                table.Reset();
            };

            /* Add the list view and the table view to the page */
            Grid grid = new Grid {};
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });

            grid.Children.Add(listView, 0, 0);
            grid.Children.Add(new ScrollView { Content = table }, 0, 1);

            Content = grid;

            // Give the page a title
            Title = TITLE;
        }

        /// <summary>
        /// Displays an error alert to the user about how an interaction couldn't save
        /// </summary>
        public void DisplayError()
        {
            DisplayAlert("Couldn't save interaction",
                "Something went wrong when saving this interaction.",
                "OK");
        }

        /// <summary>
        /// Displays an error alert to the user about how an interaction couldn't
        /// delete
        /// </summary>
        public void DisplayDeleteError()
        {
            DisplayAlert("Couldn't Remove Interaction",
                "We were not able to remove the interaction from the database.",
                "OK");
        }

        /// <summary>
        /// Displays an error alert about a selected interaction that doesn't
        /// exist in the database
        /// </summary>
        public void DisplayNonExistantError()
        {
            DisplayAlert("Reference to non-existant Interaction",
                "The interaction doesn't appear to exist in the database anymore.",
                "OK");
        }
	}
}