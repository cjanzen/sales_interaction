﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Sales_Interaction.Pages
{
    /// <summary>
    /// An abstract class that contains content contained on most
    /// pages of this application. Pages that extend this get
    /// the toolbar with the "Products" and "Settings" items.
    /// </summary>
    public abstract class GeneralPage : ContentPage
	{
        /// <summary>
        /// Constructs a Content Page with a Toolbar already filled out
        /// </summary>
        public GeneralPage ()
		{
            /* Make the toolbar */
            // Products
            ToolbarItem tbProducts = new ToolbarItem { Text = "Products" };
            tbProducts.Clicked += TbProducts_Clicked;
            ToolbarItems.Add(tbProducts);

            // Settings
            ToolbarItem tbSettings = new ToolbarItem { Text = "Settings" };
            tbSettings.Clicked += TbSettings_Clicked;
            ToolbarItems.Add(tbSettings);
        }

        /// <summary>
        /// The event handler that handles when a user taps "Settings"
        /// in the toolbar.
        /// 
        /// In this case, take the user to the Settings Page.
        /// </summary>
        /// <param name="sender">The object that sent the Settings request</param>
        /// <param name="e">Arguments for the event</param>
        private void TbSettings_Clicked(object sender, EventArgs e)
        {
            // Open the Settings Page as a modal
            Navigation.PushAsync(new SettingsPage());
        }

        /// <summary>
        /// The event handler that handles when a user taps "Products"
        /// in the toolbar.
        /// 
        /// In this case, take the user to the Products Page.
        /// </summary>
        /// <param name="sender">The object that send the Products request</param>
        /// <param name="e">Arguments for the event</param>
        private void TbProducts_Clicked(object sender, EventArgs e)
        {
            // Open the Products page
            Navigation.PushAsync(new ProductsPage());
        }
    }
}