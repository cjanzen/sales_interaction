﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales_Interaction.Models;
using Xamarin.Forms;

namespace Sales_Interaction.Pages
{
    /// <summary>
    /// A page class that represents a page that will allow
    /// users to add a new customer to the database.
    /// 
    /// Contains all the fields needed to add a customer to the database
    /// and a save button to save them.
    /// </summary>
    public class NewCustomerPage : GeneralPage
	{
        /// <summary>Page title</summary>
        public const string TITLE = "New Customer";

        /// <summary>
        /// Instantiates a new page that will allow users
        /// to add a new customer to the database with the
        /// given information.
        /// </summary>
        public NewCustomerPage ()
		{
            /* Make the Entry Cells needed for this Table View */
            // First Name
            EntryCell entFirst = new EntryCell { Label = Customer.FIRST_NAME };
            // Last Name
            EntryCell entLast = new EntryCell { Label = Customer.LAST_NAME };
            // Address
            EntryCell entAddr = new EntryCell { Label = Customer.ADDRESS };
            // Phone number
            EntryCell entPhone = new EntryCell {
                Label = Customer.PHONE,
                Keyboard = Keyboard.Telephone
            };
            // Email address
            EntryCell entEmail = new EntryCell { Label = Customer.EMAIL };

            /* Now make the Table view itself */
            TableView table = new TableView { Intent = TableIntent.Form };

            TableSection section = new TableSection("Add New Customer")
            {
                entFirst, entLast, entAddr, entPhone, entEmail
            };

            table.Root = new TableRoot { section };

            /* Now make the Save button */
            Button btnSave = new Button
            {
                Text = "Save",
                VerticalOptions = LayoutOptions.EndAndExpand,
                HorizontalOptions = LayoutOptions.Center
            };
            // Event handler that will save the info to the database
            btnSave.Clicked += (sender, e) =>
            {
                // Make a new Customer object
                Customer customer = new Customer
                {
                    FirstName = entFirst.Text,
                    LastName = entLast.Text,
                    Address = entAddr.Text,
                    Phone = entPhone.Text,
                    Email = entEmail.Text
                };

                // Save the customer to the database
                int result = App.Database.SaveItem(customer);

                // If that customer was saved successfully
                if ( result > 0 )
                {
                    // Update the Customers list from the previous page
                    List<Page> pages = Navigation.NavigationStack.ToList();
                    CustomersPage page = pages.ElementAt(pages.Count - 2) as CustomersPage;
                    page.Customers.Add(customer);
                    
                    // Pop this page out of the stack
                    Navigation.PopAsync();
                }
                // If something went wrong, Display an alert
                else
                {
                    DisplayAlert("Customer couldn't be saved",
                        "Something went wrong when saving this customer.",
                        "OK");
                }
            };

            /* Define the content */
            Content = new StackLayout
            {
				Children =
                {
					table, btnSave
				}
			};

            // Give the page a title
            Title = TITLE;
		}
	}
}