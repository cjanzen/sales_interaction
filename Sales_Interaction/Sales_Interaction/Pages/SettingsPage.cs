﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales_Interaction.Models;
using Xamarin.Forms;

namespace Sales_Interaction.Pages
{
    /// <summary>
    /// A page class that represents the settings that
    /// a user can change in the application. Right now,
    /// users can only reset their database.
    /// </summary>
    public class SettingsPage : ContentPage
	{
        /// <summary>Page title</summary>
        public const string TITLE = "Settings";

        /// <summary>
        /// Constructs a page that allows users to change
        /// the settings of the application, though at this
        /// point they can only reset the database.
        /// </summary>
        public SettingsPage ()
		{
            // Create a Reset Database button
            Button btnReset = new Button
            {
                Text = "Reset App"
            };
            btnReset.Clicked += async (sender, e) =>
            {
                // Bring up an alert asking the user if they are sure they want
                // to reset the database
                bool answer = await DisplayAlert("Reset App",
                    "Are you sure you want to reset the app's database back to defaults?",
                    "Yes", "No");

                // If the user says yes, reset the database and go to the first page
                if (answer)
                {
                    // Reset database
                    App.Database.Clear();
                    App.Database.LoadFixtures();

                    // Refresh the customers list
                    IReadOnlyList<Page> pages = Navigation.NavigationStack;
                    CustomersPage root = pages.First() as CustomersPage;
                    root.Refresh();

                    await Navigation.PopToRootAsync();
                }
            };

            // Create the label explaining the button
            Label lblReset = new Label {
                FontAttributes = FontAttributes.Bold,
                FontSize = 24,
                TextColor = Color.Red,
                Text = "WARNING: This button will reset the database to its default " +
                       "state! This means any changes you made to it will be lost!"
            };

            // Create the layout for the page
            Content = new StackLayout
            {
				Children =
                {
					btnReset, lblReset
				}
			};

            // Set the title
            Title = TITLE;
		}
	}
}