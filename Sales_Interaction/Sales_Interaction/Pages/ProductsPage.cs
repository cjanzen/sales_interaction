﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales_Interaction.Cells;
using Sales_Interaction.Models;
using Xamarin.Forms;

namespace Sales_Interaction.Pages
{
    /// <summary>
    /// A class that represents the page that will display all
    /// products in the database along with their information
    /// (including the number of interactions they're associated
    /// with).
    /// </summary>
    public class ProductsPage : GeneralPage
	{
        /// <summary>Page title</summary>
        public const string TITLE = "Products";

        /// <summary>
        /// Instantiates a page that will contain information
        /// about each product in the database, including the
        /// number of interactions they are associated with.
        /// </summary>
        public ProductsPage ()
		{
            // Remove the Products button in the toolbar
            ToolbarItems.RemoveAt(0);

            // Get a list of products for this page
            List<Product> list = App.Database.GetItems<Product>();

            // Make the appropriate list view
            ListView listView = new ListView
            {
                ItemsSource = list,
                ItemTemplate = new DataTemplate(typeof(ProductCell)),
                RowHeight = ProductCell.RowHeight,
                IsEnabled = false
            };

            Content = new StackLayout
            {
				Children =
                {
					listView
				}
			};

            // Give the page a title
            Title = TITLE;
		}
	}
}