﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Sales_Interaction.Cells;
using Sales_Interaction.Models;
using Xamarin.Forms;

namespace Sales_Interaction.Pages
{
    /// <summary>
    /// A Page class that represents the first page that users
    /// will see when they start this application. It displays
    /// the customer's name and phone number to the user.
    /// 
    /// It also allows users to add customers to the database
    /// </summary>
    public class CustomersPage : GeneralPage
	{
        /// <summary>Page title</summary>
        public const string TITLE = "Customers";

        /// <summary>Contains a list of customers</summary>
        public ObservableCollection<Customer> Customers
        {
            get { return customers; }
        }
        /// <summary>Contains a list of customers</summary>
        private ObservableCollection<Customer> customers;

        /// <summary>
        /// The list view for this page that displays customers
        /// </summary>
        private ListView listView;

        /// <summary>
        /// Builds the Customers page to display the customers and allow
        /// users to add more customers.
        /// </summary>
        public CustomersPage ()
		{
            // Create a list of customers for this page
            customers = new ObservableCollection<Customer>(App.Database.GetItems<Customer>());

            // Create the view for the list
            listView = new ListView
            {
                ItemsSource = customers,
                ItemTemplate = new DataTemplate(typeof(CustomerCell)),
                RowHeight = CustomerCell.RowHeight
            };
            listView.ItemTapped += ListView_ItemTapped;

            // Add New Customer button
            Button btnAdd = new Button
            {
                //Margin = new Thickness(10, 10),
                Text = "Add New Customer",
                VerticalOptions = LayoutOptions.End,
                HorizontalOptions = LayoutOptions.Center
            };
            btnAdd.Clicked += BtnAdd_Clicked;

            // Add the list and the button to the page via stack layout
            Content = new StackLayout
            {
				Children =
                {
					listView, btnAdd
				}
			};

            // Add the page title
            Title = TITLE;
		}

        /// <summary>
        /// The event handler for when a user clicks on an item.
        /// In this case, the user should be directed to the Customer's
        /// associated Interactions page.
        /// </summary>
        /// <param name="sender">The object that send the ItemTapped request</param>
        /// <param name="e">Arguments for the event</param>
        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // Get the selected customer
            Customer selected = e.Item as Customer;
            
            // If the customer exists
            if ( App.Database.Exists(selected) )
            {
                // Open the Interactions page with the specified Customer
                Navigation.PushAsync(new InteractionsPage(selected));
                // Deselect the item that was tapped on
                ((ListView)sender).SelectedItem = null;
            }
            else
            {
                // Otherwise, display an error message
                DisplayNonExistantError();
                // remove the selected item from the list
                customers.Remove(selected);
            }
        }

        /// <summary>
        /// Displays an error alert about a selected customer that doesn't
        /// exist in the database
        /// </summary>
        private void DisplayNonExistantError()
        {
            DisplayAlert("Reference to non-existant Customer",
                "The customer doesn't appear to exist in the database anymore.",
                "OK");
        }

        /// <summary>
        /// The event handler for when the Add button is tapped.
        /// In this case, take the user to the New Customer Page.
        /// </summary>
        /// <param name="sender">The object that send the Add request</param>
        /// <param name="e">Arguments for the event</param>
        private void BtnAdd_Clicked(object sender, EventArgs e)
        {
            // Push a new page to the stack that will allow the user to add a customer
            Navigation.PushAsync(new NewCustomerPage());
        }

        /// <summary>
        /// Refreshes the customer list so that it accurately represents in the
        /// database.
        /// </summary>
        public void Refresh()
        {
            customers = new ObservableCollection<Customer>(App.Database.GetItems<Customer>());
            listView.ItemsSource = customers;
        }

        /// <summary>
        /// Displays an alert stating that the interaction couldn't be removed
        /// </summary>
        public void DisplayDeleteError()
        {
            DisplayAlert("Couldn't Remove Customer",
                "We were not able to remove the customer from the database.",
                "OK");
        }
    }
}