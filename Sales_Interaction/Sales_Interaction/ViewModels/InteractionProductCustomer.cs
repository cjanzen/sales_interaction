﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Sales_Interaction.Models;

namespace Sales_Interaction.ViewModels
{
    /// <summary>
    /// A View Model class for the InteractionsCell whose purpose is to
    /// retrieve info about an interaction and its associated product and
    /// customer.
    /// </summary>
    public class InteractionProductCustomer : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// The View Model's Interaction that it represents
        /// </summary>
        public Interaction Interaction
        {
            get => interaction;
            set
            {
                if (interaction != value)
                {
                    interaction = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Product associated with the represented interaction
        /// </summary>
        public Product Product
        {
            get => product;
            set
            {
                if (product != value)
                {
                    product = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The Customer associated with the represented interaction
        /// </summary>
        public Customer Customer
        {
            get => customer;
            set
            {
                if (customer != value)
                {
                    customer = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        #region Attributes
        /// <summary>
        /// The View Model's Interaction that it represents
        /// </summary>
        private Interaction interaction;

        /// <summary>
        /// The Product associated with the represented interaction
        /// </summary>
        private Product product;

        /// <summary>
        /// The Customer associated with the represented interaction
        /// </summary>
        private Customer customer;
        #endregion

        #region Property Changed Event Handler
        /// <summary>
        /// The event that happens when a property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event handler that handles what happens when a property is
        /// changed
        /// </summary>
        /// <param name="property">The property that was changed (as a string)</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion

    }
}
