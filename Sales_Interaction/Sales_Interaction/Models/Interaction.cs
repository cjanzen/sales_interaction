﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales_Interaction.Database;
using SQLite;

namespace Sales_Interaction.Models
{
    /// <summary>
    /// A BindableModel class that represents a Interaction
    /// of a sale. Extends the BindableModel class to provide
    /// the ID and the ability to tell other classes that rely
    /// on this class when a property changes.
    /// </summary>
    public class Interaction : BindableModel
    {
        #region Constants
        /// <summary>Label for the Purchased indicator</summary>
        public const string PURCHASED = "Purchased?";
        /// <summary>Label for the Comments</summary>
        public const string COMMENT = "Comment:";
        /// <summary>Label for the Product</summary>
        public const string PRODUCT = "Product:";
        #endregion

        #region Properties
        /// <summary>
        /// The ID of the associated customer.
        /// </summary>
        public int CustomerID
        {
            get => customerID;
            set
            {
                if (customerID != value)
                {
                    customerID = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The date of this interaction
        /// </summary>
        public DateTime Date
        {
            get => date;
            set
            {
                if (date != value)
                {
                    date = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The comments regading this interaction
        /// </summary>
        public string Comments
        {
            get => comments;
            set
            {
                if (comments != value)
                {
                    comments = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The ID of the associated product.
        /// </summary>
        public int ProductID
        {
            get => productID;
            set
            {
                if (productID != value)
                {
                    productID = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The purchased status indicator
        /// </summary>
        public bool Purchased
        {
            get => purchased;
            set
            {
                if (purchased != value)
                {
                    purchased = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        #region Attributes
        /// <summary>
        /// The ID of the associated customer.
        /// </summary>
        private int customerID;

        /// <summary>
        /// The date of this interaction
        /// </summary>
        private DateTime date;

        /// <summary>
        /// The comments regading this interaction
        /// </summary>
        private string comments;

        /// <summary>
        /// The ID of the associated product.
        /// </summary>
        private int productID;

        /// <summary>
        /// The purchased status indicator
        /// </summary>
        private bool purchased;
        #endregion
    }
}
