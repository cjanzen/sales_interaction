﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Sales_Interaction.Database;
using SQLite;

namespace Sales_Interaction.Models
{
    /// <summary>
    /// A BindableModel class that represents a Customer
    /// of a sale. Extends the BindableModel class to provide
    /// the ID and the ability to tell other classes that rely
    /// on this class when a property changes.
    /// </summary>
    public class Customer : BindableModel
    {
        #region Constants
        /// <summary>The label for the first name</summary>
        public const string FIRST_NAME = "First Name";
        /// <summary>The label for the last name</summary>
        public const string LAST_NAME = "Last Name";
        /// <summary>The label for the address</summary>
        public const string ADDRESS = "Address";
        /// <summary>The label for the Phone number</summary>
        public const string PHONE = "Phone";
        /// <summary>The label for the email address</summary>
        public const string EMAIL = "Email";
        #endregion

        #region Properties
        /// <summary>
        /// Customer's first name.
        /// </summary>
        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (firstName != value)
                {
                    firstName = value;
                    OnPropertyChanged();
                    OnPropertyChanged("FullName");
                }
            }
        }

        /// <summary>
        /// Customer's last name.
        /// </summary>
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    OnPropertyChanged();
                    OnPropertyChanged("FullName");
                }
            }
        }

        /// <summary>
        /// Customer's address.
        /// </summary>
        public string Address
        {
            get { return address; }
            set
            {
                if (address != value)
                {
                    address = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Customer's phone number.
        /// </summary>
        public string Phone
        {
            get { return phone; }
            set
            {
                if (phone != value)
                {
                    phone = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Customer's email address.
        /// </summary>
        public string Email
        {
            get { return email; }
            set
            {
                if (email != value)
                {
                    email = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the full name of the customer
        /// </summary>
        [Ignore]
        public string FullName
        {
            get { return firstName + " " + lastName; }
        }
        #endregion

        #region Attributes
        /// <summary>
        /// Customer's first name.
        /// </summary>
        private string firstName;

        /// <summary>
        /// Customer's last name.
        /// </summary>
        private string lastName;

        /// <summary>
        /// Customer's address.
        /// </summary>
        private string address;

        /// <summary>
        /// Customer's phone number.
        /// </summary>
        private string phone;

        /// <summary>
        /// Customer's email address.
        /// </summary>
        private string email;
        #endregion

    }
}
