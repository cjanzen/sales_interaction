﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales_Interaction.Database;

namespace Sales_Interaction.Models
{
    /// <summary>
    /// A BindableModel class that represents a Product
    /// of a sale. Extends the BindableModel class to provide
    /// the ID and the ability to tell other classes that rely
    /// on this class when a property changes.
    /// </summary>
    public class Product : BindableModel
    {
        #region Properties
        /// <summary>Product's Name</summary>
        public string ProductName
        {
            get => productName;
            set
            {
                if ( productName != value )
                {
                    productName = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>Product's description</summary>
        public string Description
        {
            get => description;
            set
            {
                if (description != value)
                {
                    description = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>Product's Price</summary>
        public double Price
        {
            get => price;
            set
            {
                if (price != value)
                {
                    price = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

        #region Attributes
        /// <summary>Product's Name</summary>
        private string productName;
        /// <summary>Product's description</summary>
        private string description;
        /// <summary>Product's Price</summary>
        private double price;
        #endregion
    }
}
