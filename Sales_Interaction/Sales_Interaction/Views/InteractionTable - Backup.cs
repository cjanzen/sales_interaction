﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using A1_cst219.Models;
using A1_cst219.Pages;
using Xamarin.Forms;

namespace A1_cst219.Views
{
    public class InteractionTable : TableView
    {
        #region Constants
        private const string ADD_TITLE = "Add Interaction";
        private const string MOD_TITLE = "Modify Interaction";

        private const string ADD_BTN = "Add";
        private const string MOD_BTN = "Save";
        #endregion

        #region Attributes
        private Interaction interaction;
        private Customer customer;
        private ObservableCollection<Interaction> list;
        private DatePicker date;
        private EntryCell entComment;
        private Picker product;
        private Switch swPurchase;
        private TableSection section;
        private Button btnAdd;
        #endregion

        public InteractionTable(ObservableCollection<Interaction> list, Customer customer)
        {
            // Assign the specific list and customer that we're dealing with
            this.list = list;
            this.customer = customer;

            // Give the table an Intent
            Intent = TableIntent.Form;

            /* Make a table section for the Interaction */
            // Date
            date = new DatePicker { Date = DateTime.Now };
            // Comment
            entComment = new EntryCell { Label = Interaction.COMMENT };
            // Product
            Label lblProd = new Label { Text = Interaction.PRODUCT };
            product = new Picker {
                //ItemsSource = App.Database.GetItems<Product>(),
                ItemDisplayBinding = new Binding("ProductName")
            };
            StackLayout slProduct = new StackLayout {
                Orientation = StackOrientation.Horizontal,
                Children = { lblProd, product }
            };
            // Purchase
            Label lblPurchase = new Label {
                Text = Interaction.PURCHASED
            };
            swPurchase = new Switch {
                HorizontalOptions = LayoutOptions.EndAndExpand
            };
            StackLayout slPurchase = new StackLayout {
                Orientation = StackOrientation.Horizontal,
                Children = { lblPurchase, swPurchase }
            };
            // Add/Save button
            btnAdd = new Button();
            btnAdd.Clicked += BtnAdd_Clicked;

            section = new TableSection {
                new ViewCell{ View = date },
                entComment,
                new ViewCell{ View = slProduct },
                new ViewCell{ View = slPurchase },
                new ViewCell{ View = btnAdd },
            };

            // Reset everything to defaults
            Reset();

            // Add section to root
            Root = new TableRoot { section };
        }

        private void Reset()
        {
            // Reset cells to their defaults
            date.Date = DateTime.Now;
            entComment.Text = "";
            product.ItemsSource = App.Database.GetItems<Product>();
            product.SelectedIndex = 1;
            swPurchase.IsToggled = false;
            btnAdd.Text = ADD_BTN;

            // Reset section title
            section.Title = ADD_TITLE;

            // Reset interaction to null customer
            interaction = null;
        }

        public void Modify(Interaction interaction)
        {
            // Set the interaction for this table
            this.interaction = interaction;
            
            // Get interactions's product
            Product iProduct = App.Database.GetItem(new Product { ID = interaction.ProductID });
            
            // Set the cells so that they display interaction's info
            date.Date = interaction.Date;
            entComment.Text = interaction.Comments;
            product.SelectedIndex = 0;
            swPurchase.IsToggled = interaction.Purchased;
            btnAdd.Text = MOD_BTN;
        }

        private void BtnAdd_Clicked(object sender, EventArgs e)
        {
            bool isNew = interaction == null;

            // If this is a new interaction, create the interaction object
            if ( isNew )
            {
                interaction = new Interaction();
            }

            // Set interaction's properties to the properties specified by the user
            interaction.Date = date.Date;
            interaction.Comments = entComment.Text;
            interaction.ProductID = ((Product)product.SelectedItem).ID;
            interaction.Purchased = swPurchase.IsToggled;
            interaction.CustomerID = customer.ID;

            // Save the interaction to the database
            int result = App.Database.SaveItem(interaction);

            // If the interaction saved successfully
            if ( result > 0 )
            {
                // Add the interaction to the list if it is new
                if (isNew) list.Add(interaction);

                // Reset the table
                Reset();
            }
            else
            {
                // Display an alert
                ((InteractionsPage)Parent).DisplayError();
            }
        }
    }
}
