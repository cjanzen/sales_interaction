﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Sales_Interaction.Models;
using Sales_Interaction.Pages;
using Sales_Interaction.ViewModels;
using Xamarin.Forms;

namespace Sales_Interaction.Views
{
    /// <summary>
    /// A TableView class that allows users to create or edit
    /// interactions based on the item they selected in the
    /// ListView of the Interactions Page.
    /// 
    /// They can edit all properties of the interaction except
    /// for the ID
    /// </summary>
    public class InteractionTable : TableView
    {
        #region Constants
        /// <summary>The section title for adding an Interaction</summary>
        private const string ADD_TITLE = "Add Interaction";
        /// <summary>The section title for modifying an Interaction</summary>
        private const string MOD_TITLE = "Modify Interaction";
        /// <summary>Button label for adding</summary>
        private const string ADD_BTN = " Add ";
        /// <summary>Button label for saving</summary>
        private const string MOD_BTN = "Save";

        /// <summary>The height of the product and purchased cells</summary>
        private const int HEIGHT = 53;
        /// <summary>The amount of padding on the left-side of the product and purchased cells</summary>
        private const double LEFT_PAD = 14;
        #endregion

        #region Attributes
        /// <summary>The current customer we're dealing with</summary>
        private Customer customer;
        /// <summary>The parent page that contains this table</summary>
        private InteractionsPage parent;

        /* Cells and views */
        /// <summary>Date picker for dates</summary>
        private DatePicker date;
        /// <summary>Text box for comments</summary>
        private EntryCell entComment;
        /// <summary>Picker for products</summary>
        private Picker pProduct;
        /// <summary>Switch for the Purchased indicator</summary>
        private Switch swPurchased;
        /// <summary>The add/save button</summary>
        private Button btnAdd;

        /// <summary>
        /// The only section for this table that allows the user
        /// to add/modify an interaction
        /// </summary>
        private TableSection section;
        #endregion

        #region Constructor
        /// <summary>
        /// Builds the table that allows users to create/edit interactions
        /// based on the selected item from the parent's ListView.
        /// </summary>
        /// <param name="parent">The Interactions page associated with
        /// this table</param>
        /// <param name="customer">The customer associated with the
        /// interaction</param>
        public InteractionTable(InteractionsPage parent, Customer customer)
        {
            // Setup the attributes for this object
            this.customer = customer;
            this.parent = parent;
            
            /* Make the table section for this table */
            // Date
            date = new DatePicker { Date = DateTime.Now, Format = "dddd, MMMM dd, yyyy" };
            // Comment
            entComment = new EntryCell { Label = Interaction.COMMENT };
            // Product
            pProduct = new Picker
            {
                ItemsSource = App.Database.GetItems<Product>(),
                SelectedIndex = 0,
                ItemDisplayBinding = new Binding("ProductName"),
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            StackLayout slProduct = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Padding = new Thickness(LEFT_PAD, 0, 0, 0),
                Children =
                {
                    new Label
                    {
                        Text = Interaction.PRODUCT,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.End
                    },
                    pProduct
                }
            };
            // Purchased
            swPurchased = new Switch
            {
                HorizontalOptions = LayoutOptions.EndAndExpand
            };
            StackLayout slPurchased = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(LEFT_PAD, 0, 0, 0),
                Children = {
                    new Label {
                        Text = Interaction.PURCHASED,
                        VerticalTextAlignment = TextAlignment.Center,
                        HorizontalTextAlignment = TextAlignment.End
                    },
                    swPurchased
                }
            };
            // Add/Save button
            btnAdd = new Button { Text = ADD_BTN, HorizontalOptions = LayoutOptions.Center };
            btnAdd.Clicked += BtnAdd_Clicked;

            // Now put the table section together!
            ViewCell vDate = new ViewCell { View = date };
            ViewCell vProduct = new ViewCell { View = slProduct };
            ViewCell vPurchased = new ViewCell { View = slPurchased };
            ViewCell vBtn = new ViewCell { View = btnAdd };

            section = new TableSection(ADD_TITLE)
            {
                vDate,
                entComment,
                vProduct,
                vPurchased,
                vBtn
            };

            // Set the height so as to only take up as much room as needed
            RowHeight = HEIGHT;
            #if __IOS__
            HeightRequest = HEIGHT * 5.5;
            #endif
            #if __ANDROID__
            HeightRequest = HEIGHT * 6.5;
            #endif

            // Add the table section to the root
            Root = new TableRoot { section };

            // Set all fields to default
            Reset();
        }
        #endregion

        /// <summary>
        /// Resets all fields back to their default state
        /// </summary>
        public void Reset()
        {
            // Reset all fields to default state
            date.Date = DateTime.Now;
            entComment.Text = "";
            pProduct.ItemsSource = App.Database.GetItems<Product>();
            pProduct.SelectedIndex = 0;
            swPurchased.IsToggled = false;
            btnAdd.Text = ADD_BTN;

            // Change section title back to add interaction
            section.Title = ADD_TITLE;

            // Deselect whatever was selected in the list view
            try
            {
                parent.List.SelectedItem = null;
            }
            catch (NullReferenceException nfe) { } // Do nothing if the list doesn't exist yet
            
        }

        /// <summary>
        /// Modifies the fields in the table based on the given Interaction
        /// </summary>
        /// <param name="intAction">The ineraction that was selected</param>
        public void Modify()
        {
            // Find the selected item
            Interaction intAction = ((InteractionProductCustomer)parent.List.SelectedItem).Interaction;
            
            // Update all fields to reflect the selected interaction
            date.Date = intAction.Date;
            entComment.Text = intAction.Comments;
            pProduct.SelectedIndex = FindIndex(intAction.ProductID);
            swPurchased.IsToggled = intAction.Purchased;
            btnAdd.Text = MOD_BTN;

            // Change section title to modify interaction
            section.Title = MOD_TITLE;
        }

        /// <summary>
        /// Finds the index of a product with the given id within the products picker
        /// </summary>
        /// <param name="id">The id of a product</param>
        /// <returns>The index that the product exists in (or -1 if it doesn't exist)</returns>
        private int FindIndex(int id)
        {
            int result = -1;
            int i = -1; // Index of the list
            Product[] products = ((List<Product>)pProduct.ItemsSource).ToArray();

            // For each index in the products picker
            while ( result == -1 && ++i < products.Length )
            {
                // Get the current product
                Product product = products[i];
                
                // If the IDs match
                if ( product.ID == id )
                {
                    // Set the result to the current index
                    result = i;
                }
            }

            return result;
        }

        /// <summary>
        /// The event handler that handles the event where the user taps
        /// the Add/Save button. In this case, add or save the interaction
        /// depending on whether the user selected an item or not.
        /// </summary>
        /// <param name="sender">The object that sent the Add/Save request</param>
        /// <param name="e">The Event arguments</param>
        private void BtnAdd_Clicked(object sender, EventArgs e)
        {
            // Get the currently selected item
            InteractionProductCustomer intAction = parent.List.SelectedItem as InteractionProductCustomer;
            
            // Check if this interaction is new
            bool isNew = intAction == null;

            // If this interaction is new, create it
            if (isNew)
            {
                intAction = new InteractionProductCustomer();
                intAction.Interaction = new Interaction();
            }

            // Set all properties of this interaction (except ID)
            intAction.Interaction.CustomerID = customer.ID;
            intAction.Interaction.Date = date.Date;
            intAction.Interaction.Comments = entComment.Text;
            intAction.Interaction.ProductID = ((Product)pProduct.SelectedItem).ID;
            intAction.Interaction.Purchased = swPurchased.IsToggled;

            // Set the product for the view
            intAction.Product = App.Database.GetItem((Product)pProduct.SelectedItem);

            // Set the customer for this view
            intAction.Customer = customer;

            // Save the Interaction to the database
            int result = App.Database.SaveItem(intAction.Interaction);

            // If there were no issues with the saving
            if ( result > 0 )
            {
                // If the interaction is new, add it to the list
                if (isNew) parent.Interactions.Add(intAction);

                // Reset fields back to default
                Reset();
            }
            else
            {
                // Display an error message
                parent.DisplayError();
            }
        }

    }
}
