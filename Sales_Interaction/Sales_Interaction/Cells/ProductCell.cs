﻿using System;
using System.Collections.Generic;
using System.Text;
using Sales_Interaction.Models;
using Xamarin.Forms;

namespace Sales_Interaction.Cells
{
    /// <summary>
    /// A ViewCell class used to represent a Product.
    /// 
    /// Displays name, description, price, and number
    /// of interactions
    /// </summary>
    public class ProductCell : ViewCell
    {
        /// <summary>
        /// The height for each Customer Cell
        /// </summary>
        public const int RowHeight = 90;

        /// <summary>
        /// The font size for the labels
        /// </summary>
        private const double FONT_SIZE = 16;

        #region Attributes
        /// <summary>Label for the interactions count</summary>
        private Label lblInts;
        #endregion

        /// <summary>
        /// Instantiates a ProductCell object that displays
        /// product information.
        /// </summary>
        public ProductCell()
        {
            /* Setup the bindings */
            // Name
            Label lblName = new Label
            {
                FontSize = FONT_SIZE,
                FontAttributes = FontAttributes.Bold,
            };
            lblName.SetBinding(Label.TextProperty, "ProductName");

            // Description
            Label lblDesc = new Label
            {
                FontSize = FONT_SIZE
            };
            lblDesc.SetBinding(Label.TextProperty, "Description");

            // Price
            Label lblPrice = new Label
            {
                FontSize = FONT_SIZE,
                HorizontalOptions = LayoutOptions.EndAndExpand
            };
            lblPrice.SetBinding(Label.TextProperty, "Price", stringFormat: "{0:C2}");

            // Num of Interactions
            lblInts = new Label
            {
                FontSize = FONT_SIZE
            };

            /* Setup the View */
            View = new StackLayout
            {
                Spacing = 2,
                Padding = 5,
                Children = {
                    lblName,
                    new StackLayout {
                        Orientation = StackOrientation.Horizontal,
                        Children = { lblDesc, lblPrice }
                    },
                    lblInts
                }
            };
        }

        /// <summary>
        /// An event handler for when the Bounded object has been changed.
        /// Needed to include this so that the number of interactions for
        /// a product could be displayed.
        /// </summary>
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            // Set the interaction label's text to the count of interactions for
            // this product
            Product product = BindingContext as Product;
            lblInts.Text = "Interactions: " + App.Database.CountProductInteractions(product);
        }
    }
}
