﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Text;
using Sales_Interaction.Models;
using Sales_Interaction.Pages;
using Sales_Interaction.ViewModels;
using Xamarin.Forms;

namespace Sales_Interaction.Cells
{
    /// <summary>
    /// A ViewCell that represents an interaction and its
    /// associated customer and product.
    /// 
    /// Displays Customer name, interaction date,
    /// interaction comment, product name, and Purchased
    /// indicator.
    /// 
    /// Note that this class assumes that
    /// you have binded InteractionProductCustomer objects
    /// to this instead of the regular Interactions object.
    /// </summary>
    public class InteractionsCell : ViewCell
    {
        /// <summary>
        /// The height for each Customer Cell
        /// </summary>
        public const int RowHeight = 90;

        /// <summary>
        /// The font size for the labels
        /// </summary>
        private const double FONT_SIZE = 16;

        /// <summary>
        /// Instantiates a InteractionsCell that displays information
        /// about an interaction. Note that this class assumes that
        /// you have binded InteractionProductCustomer objects
        /// to this instead of the regular Interactions object.
        /// </summary>
        public InteractionsCell()
        {
            /* Setup the bindings */
            // Customer name
            Label lblCustName = new Label
            {
                FontAttributes = FontAttributes.Bold,
                FontSize = FONT_SIZE
            };
            lblCustName.SetBinding(Label.TextProperty, "Customer.FullName");

            // Interaction Date
            Label lblDate = new Label
            {
                FontSize = FONT_SIZE,
            };
            lblDate.SetBinding(Label.TextProperty, "Interaction.Date",
                stringFormat: "{0:dddd, MMMM dd, yyyy}");

            // Interaction Comments
            Label lblComment = new Label
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                FontSize = 16,
            };
            lblComment.SetBinding(Label.TextProperty, "Interaction.Comments");

            // Product Name
            Label lblProdName = new Label
            {
                FontSize = FONT_SIZE,
            };
            lblProdName.SetBinding(Label.TextProperty, "Product.ProductName");

            // Purchased
            Label lblPurchases = new Label
            {
                Text = Interaction.PURCHASED,
                FontSize = FONT_SIZE
            };

            Switch swPurchased = new Switch();
            swPurchased.SetBinding(Switch.IsToggledProperty, "Interaction.Purchased");
            swPurchased.Toggled += SwPurchased_Toggled;

            StackLayout slPurchased = new StackLayout
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Orientation = StackOrientation.Horizontal,
                Children = { lblPurchases, swPurchased }
            };

            /* Setup the View */
            View = new StackLayout
            {
                Spacing = 2,
                Padding = 5,
                Children = {
                lblCustName,
                new StackLayout
                {
                    Orientation = StackOrientation.Horizontal,
                    Children = { lblDate, lblComment }
                },
                new StackLayout
                {
                    Orientation = StackOrientation.Horizontal,
                    Children = { lblProdName, slPurchased }
                },
            }
            };

            /* Context Menu */
            // Delete
            MenuItem mDelete = new MenuItem { Text = "Delete", IsDestructive = true };
            mDelete.Clicked += MDelete_Clicked;
            ContextActions.Add(mDelete);
        }

        /// <summary>
        /// The event handler that is triggered when the Purchased toggle has
        /// been toggled
        /// </summary>
        /// <param name="sender">The object that send the Toggle request</param>
        /// <param name="e">Arguments for the event</param>
        private void SwPurchased_Toggled(object sender, ToggledEventArgs e)
        {
            // Get the current interaction
            Interaction interaction =
                ((InteractionProductCustomer)BindingContext).Interaction;

            // If the current interaction exists
            if ( App.Database.Exists(interaction) )
            {
                // Save the toggled state to the database
                App.Database.SaveItem(interaction);
            }
            else
            {
                // Get the parent page
                InteractionsPage page = Parent.Parent.Parent as InteractionsPage;
                // Display an error about the non-existent interaction
                page.DisplayNonExistantError();
                // Remove the item in the list corresponding to this interaction
                page.Interactions.Remove(((InteractionProductCustomer)BindingContext));
            }
        }

        /// <summary>
        /// This event is triggered when the user selects
        /// Delete from the Context Menu.
        /// </summary>
        /// <param name="sender">The object that send the Delete request</param>
        /// <param name="e">Arguments for the event</param>
        private void MDelete_Clicked(object sender, EventArgs e)
        {
            // Get the interaction from the view model
            Interaction interaction = ((InteractionProductCustomer)BindingContext).Interaction;
            
            // Remove Interaction from database
            int result = App.Database.DeleteItemAsync(interaction);

            // Remove the customer from the list
            ListView parent = (ListView)this.Parent;
            ObservableCollection<InteractionProductCustomer> list =
                (ObservableCollection<InteractionProductCustomer>)parent.ItemsSource;
            list.Remove(BindingContext as InteractionProductCustomer);
        }

        /// <summary>
        /// An event handler for when the Bounded object has been changed.
        /// Needed to include this because an exception is triggered whenever
        /// an interaction is deleted. The try-catch block prevents the app
        /// from crashing.
        /// </summary>
        protected override void OnBindingContextChanged()
        {
            try
            {
                base.OnBindingContextChanged();
            }
            catch (NullReferenceException nre) { } // Do nothing if it's a null reference
        }
    }
}
