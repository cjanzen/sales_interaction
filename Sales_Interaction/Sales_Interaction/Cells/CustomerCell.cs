﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Sales_Interaction.Models;
using Sales_Interaction.Pages;
using Xamarin.Forms;

namespace Sales_Interaction.Cells
{
    /// <summary>
    /// A ViewCell class that represents a Customer.
    /// Displays Full Name and Phone number.
    /// </summary>
    public class CustomerCell : ViewCell
    {
        /// <summary>
        /// The height for each Customer Cell
        /// </summary>
        public const int RowHeight = 55;

        /// <summary>
        /// The font size for the labels
        /// </summary>
        #if __ANDROID__
        private const double FONT_SIZE = 32;
        #endif
        #if __IOS__
        private const double FONT_SIZE = 24;
        #endif

        /// <summary>
        /// Instantiates a Customer Cell to display a customers
        /// information in a List View
        /// </summary>
        public CustomerCell()
        {
            /* Setup the bindings */
            // Customer Name
            Label lblName = new Label
            {
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.Start,
                FontSize = FONT_SIZE
            };
            lblName.SetBinding(Label.TextProperty, "FullName");

            // Customer Phone Number
            Label lblPhone = new Label
            {
                HorizontalOptions = LayoutOptions.EndAndExpand,
                HorizontalTextAlignment = TextAlignment.End,
                FontSize = FONT_SIZE
            };
            lblPhone.SetBinding(Label.TextProperty, "Phone");

            /* Setup the View */
            View = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 2,
                Padding = 5,
                Children = { lblName, lblPhone }
            };

            /* Context Menu */
            // Delete
            MenuItem mDelete = new MenuItem { Text = "Delete", IsDestructive = true };
            mDelete.Clicked += MDelete_Clicked;
            ContextActions.Add(mDelete);
        }

        /// <summary>
        /// This event is triggered when the user selects
        /// Delete from the Context Menu.
        /// </summary>
        /// <param name="sender">The object that send the Delete request</param>
        /// <param name="e">Arguments for the event</param>
        private void MDelete_Clicked(object sender, EventArgs e)
        {
            // Remove all interactions from customer
            App.Database.DeleteInteractionsFromCustomer(BindingContext as Customer);

            // Remove customer from database
            App.Database.DeleteItemAsync(BindingContext as Customer);

            // Remove the customer from the list
            ListView parent = (ListView)this.Parent;
            ObservableCollection<Customer> list =
                (ObservableCollection<Customer>)parent.ItemsSource;
            list.Remove(this.BindingContext as Customer);
        }
    }
}
