﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Sales_Interaction.Models;
using Sales_Interaction.ViewModels;

namespace Sales_Interaction.Database
{
    /// <summary>
    /// A class whose purpose is to provide project-specific database
    /// functionality. It extends the DBHelper class to provide
    /// general-purpose functionality (ex. Getting Items,
    /// clearing the database, etc.)
    /// </summary>
    public class SalesHelper : DBHelper
    {

        /// <summary>
        /// Constructs a DBHelper object that deals with this
        /// project specifically.
        /// </summary>
        /// <param name="path">The path to the database</param>
        public SalesHelper(string path) : base(path)
        {
        }

        /// <summary>
        /// Initiate any tables that need to be created
        /// </summary>
        protected override void InitTables()
        {
            // No FKs
            database.CreateTable<Customer>();
            database.CreateTable<Product>();

            // With FKs
            database.CreateTable<Interaction>();
        }

        /// <summary>
        /// Loads any fixtures for testing purposes
        /// </summary>
        public override void LoadFixtures()
        {
            int jacketId = 0, hatId = 0, bootsId = 0;//,
            //  jasonId = 0, ernId = 0; 


            // Customer fixtures
            //if (IsEmpty<Customer>())
            //{
            //    jasonId = SaveItem(new Customer { FirstName = "Jason", LastName = "Schmidt",
            //        Phone = "306-659-4629" });
            //    ernId = SaveItem(new Customer { FirstName = "Ernesto", LastName = "Basoalto",
            //        Phone = "306-659-4124" });
            //}

            // If the Products table is empty
            if (IsEmpty<Product>())
            {
                // Load Product fixtures
                jacketId = SaveItem(new Product { ProductName = "Wonder Jacket",
                    Description = "A wonderful jacket", Price = 499.99 });
                hatId = SaveItem(new Product { ProductName = "Wonder Hat",
                    Description = "A wonderful hat", Price = 124.99 });
                bootsId = SaveItem(new Product { ProductName = "Wonder Boots",
                    Description = "A wonderful pair of high quality boots",
                    Price = 224.99 });
            }

            // Interactions fixtures
            //if (IsEmpty<Interaction>())
            //{
            //    SaveItem(new Interaction { CustomerID = jasonId, Date = new DateTime(2018, 1, 19),
            //        Comments = "Good Talk!", ProductID = hatId, Purchased = true });
            //    SaveItem(new Interaction { CustomerID = jasonId, Date = new DateTime(2018, 1, 20),
            //        Comments = "Didn't Go Well", ProductID = jacketId, Purchased = false });
            //    SaveItem(new Interaction { CustomerID = jasonId, Date = new DateTime(2018, 1, 20),
            //        Comments = "Almost sold it!", ProductID = hatId, Purchased = false });
            //}
        }

        /// <summary>
        /// Clears out the entire database by dropping tables,
        /// then reinstantiates the lost tables.
        /// </summary>
        public override void Clear()
        {
            // With FKs
            database.DropTable<Interaction>();

            // No FKs
            database.DropTable<Customer>();
            database.DropTable<Product>();

            // Reinitialize the tables
            InitTables();
        }

        #region Custom Queries
        /// <summary>
        /// Gets a list of interactions for the specified customer as InteractionProductCustomer objects
        /// </summary>
        /// <param name="customer">The customer you want to get the interactions for</param>
        /// <returns>A list of interactions for the specified customer as InteractionProductCustomer objectsr</returns>
        public List<InteractionProductCustomer> GetCustomersInteractions(Customer customer)
        {
            List<InteractionProductCustomer> intProdCusts = new List<InteractionProductCustomer>();

            // Get a List of interactions
            List<Interaction> interactions =
                database.Table<Interaction>().Where(e => e.CustomerID == customer.ID).ToList();

            // For every interaction, fill in the associated view model with
            // the associated customer and product
            foreach( Interaction interaction in interactions )
            {
                // Create the view model object
                InteractionProductCustomer intProdCust = new InteractionProductCustomer
                {
                    Interaction = interaction,
                    Product = App.Database.GetItem<Product>(new Product { ID = interaction.ProductID }),
                    Customer = customer
                };

                // Add it to the intProdCusts list
                intProdCusts.Add(intProdCust);
            }

            return intProdCusts;
        }

        /// <summary>
        /// Gets the number of interations for a product
        /// </summary>
        /// <param name="product">The product that you want to count the interactions for</param>
        /// <returns>the number of interations for a product</returns>
        public int CountProductInteractions(Product product)
        {
            return database.Table<Interaction>().Where(e => e.ProductID == product.ID).Count();
        }


        /// <summary>
        /// Deletes all interactions involved with a customer
        /// </summary>
        /// <param name="customer">The customer that you want to remove all interactions from</param>
        /// <returns>The number of rows affected by the delete</returns>
        public int DeleteInteractionsFromCustomer(Customer customer)
        {
            return database.Table<Interaction>().Delete(e => e.CustomerID == customer.ID);
        }
        #endregion
    }
}
