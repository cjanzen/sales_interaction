﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using SQLite;

namespace Sales_Interaction.Database
{
    /// <summary>
    /// An abstract class used to represent Models that could be binded
    /// to a Cell or whatever property. It defines the ID and event
    /// handlers for when a property is changed, so every class that
    /// inherits from this class will have these included.
    /// </summary>
    public abstract class BindableModel : INotifyPropertyChanged
    {
        #region Properties
        /// <summary>
        /// ID of this Model object
        /// </summary>
        [PrimaryKey, AutoIncrement]
        public int ID
        {
            get => id;
            set
            {
                if (id != value)
                {
                    id = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// The ID of this Model object. Only accessible
        /// by its children.
        /// </summary>
        protected int id;
        #endregion

        #region Property Changed Event Handler
        /// <summary>
        /// The event that happens when a property is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event handler that handles what happens when a property is
        /// changed
        /// </summary>
        /// <param name="property">The property that was changed (as a string)</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string property = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
            }
            catch (Exception e) { }
        }
        #endregion
    }
}
