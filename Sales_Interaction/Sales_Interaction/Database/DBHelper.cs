﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SQLite;

namespace Sales_Interaction.Database
{
    /// <summary>
    /// An abstract class whose purpose is to generalize all functionality
    /// of the database so that it can be portable from project to project.
    /// The class can be extended to provide project-specific functionality
    /// as needed (ex. Customer Queries).
    /// </summary>
    public abstract class DBHelper
    {
        /// <summary>The connection to the database</summary>
        protected readonly SQLiteConnection database;

        /// <summary>
        /// Creates the connection to the database so that you can communicate with it
        /// </summary>
        /// <param name="path">File path to the database</param>
        /// <param name="loadFixtures">Loads fixtures into the database if there are no entries in the database</param>
        /// <param name="cleanDatabase">Clears out the database upon loading the app</param>
        public DBHelper(string path)
        {
            // Create a connection
            database = new SQLiteConnection(path);

            // Enable foreign keys
            database.Execute("PRAGMA foreign_keys = ON");

            // Initiate any tables that need to be created
            InitTables();
        }

        /// <summary>
        /// Initiate any tables that need to be created
        /// </summary>
        protected abstract void InitTables();

        /// <summary>
        /// Loads any fixtures for testing purposes
        /// </summary>
        public abstract void LoadFixtures();

        /// <summary>
        /// Saves a BindableModel object into the database.
        /// </summary>
        /// <typeparam name="T">Only BindableModel objects are allowed here.</typeparam>
        /// <param name="item">The object you want to save into the database</param>
        /// <returns>The number of rows updated if an update happened,
        /// the ID object if an insert happened</returns>
        public int SaveItem<T>(T item) where T:BindableModel
        {
            // If this not a new item, update the table entry
            if ( item.ID != 0 )
            {
                return database.Update(item);
            }
            // If this is a new item, save it to the database and update the ID of the object
            else
            {
                database.Insert(item);
                return item.ID;
            }
        }

        /// <summary>
        /// Determines if the specified table has any entries in the database
        /// </summary>
        /// <typeparam name="T">The table you want to check</typeparam>
        /// <returns>True if its empty, false otherwise</returns>
        public bool IsEmpty<T>() where T : new()
        {
            return database.Table<T>().Count() == 0;
        }

        /// <summary>
        /// Clears out the entire database
        /// </summary>
        public abstract void Clear();

        /// <summary>
        /// Deletes an item from the database
        /// </summary>
        /// <typeparam name="T">The table that your deleting from</typeparam>
        /// <param name="item">The item you want to delete</param>
        /// <returns></returns>
        public int DeleteItemAsync<T>(T item) where T : BindableModel
        {
            return database.Delete(item);
        }

        /// <summary>
        /// Returns all of the items from a table as a List
        /// </summary>
        /// <typeparam name="T">The table you want to get items from</typeparam>
        /// <returns>All the items from the specified table</returns>
        public List<T> GetItems<T>() where T : new()
        {
            return database.Table<T>().ToList<T>();
        }

        /// <summary>
        /// Gets a specific item from the database
        /// </summary>
        /// <typeparam name="T">The table from which you want the item from</typeparam>
        /// <param name="item">The item with the ID prefilled (The ID should be the id of the item you're looking for)</param>
        /// <returns>The item with all fields filled out from the database</returns>
        public T GetItem<T>(T item) where T : BindableModel
        {
            // Use reflection to run the method, since I can't use the normal method of doing so
            Type[] types = new Type[] { item.GetType() };
            TableQuery<T> query = typeof(SQLiteConnection).GetMethod("Table").
                MakeGenericMethod(types).Invoke(database, null) as TableQuery<T>;

            // Return the first entry from the query
            return query.Where(i => i.ID == item.ID).FirstOrDefault();
        }

        /// <summary>
        /// Checks if the specified item exists in the database, based on ID
        /// </summary>
        /// <typeparam name="T">The table from which the item comes from</typeparam>
        /// <param name="item">The item with the ID prefilled (The ID should be the
        /// id of the item you're looking for)</param>
        /// <returns>True if the item exists, false otherwise</returns>
        public bool Exists<T>(T item) where T : BindableModel
        {
            // Use reflection to run the method, since I can't use the normal method of doing so
            Type[] types = new Type[] { item.GetType() };
            TableQuery<T> query = typeof(SQLiteConnection).GetMethod("Table").
                MakeGenericMethod(types).Invoke(database, null) as TableQuery<T>;

            // Return the first entry from the query
            return query.Where(i => i.ID == item.ID).Count() == 1;
        }
    }
}
