﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sales_Interaction.Database;
using Xamarin.Forms;

namespace Sales_Interaction
{
	public partial class App : Application
	{
        /// <summary>
        /// The database connection this app will use to persist
        /// data
        /// </summary>
        static SalesHelper database;
        /// <summary>
        /// The database connection this app will use to persist
        /// data
        /// </summary>
        public static SalesHelper Database
        {
            get
            {
                // If the database connection hasn't been made, make it
                if ( database == null )
                {
                    database = new SalesHelper(DependencyService.Get<IFileHelper>().GetLocalFilePath("SalesDB.db3"));
                }

                return database;
            }
        }

        /// <summary>
        /// The starting code for the app. Should initialize components,
        /// create the database connection, load the product fixtures,
        /// and load the Main Page (which is the customer's page)
        /// </summary>
        public App ()
		{
			InitializeComponent();

            database = Database;

            // Load any fixtures that we need
            //database.Clear();
            database.LoadFixtures();

			MainPage = new NavigationPage(new Sales_Interaction.Pages.CustomersPage());
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
