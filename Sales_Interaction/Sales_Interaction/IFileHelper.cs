﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales_Interaction
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }
}
